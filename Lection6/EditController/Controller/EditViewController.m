//
//  EditViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "EditViewController.h"
#import "EditView.h"

@interface EditViewController () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, strong) EditView *editView;

@end

@implementation EditViewController

- (void)loadView {
    self.editView = [[EditView alloc] init];
    self.view = self.editView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.item) {
        self.item = [[Item alloc] init];
    }
    
    self.editView.titleTextField.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveItem:)];
    
    
    self.editView.titleTextField.text = self.item.title;
    self.editView.detailTextView.text = self.item.detail;
    [self.editView.iconButton setImage:[UIImage imageNamed:self.item.imageName] forState:UIControlStateNormal];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.editView.topOffset = self.topLayoutGuide.length;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return text.length <= 10;
}

- (void)saveItem:(id)sender {
    self.item.title = self.editView.titleTextField.text;
    self.item.detail = self.editView.detailTextView.text;
    self.item.imageName = @"0";
    
    if (self.delegate) {
        [self.delegate editViewController:self didCreateItem:self.item];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
